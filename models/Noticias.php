<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "noticias".
 *
 * @property int $id_noticia
 * @property string $titulo
 * @property string $texto
 * @property string $foto
 * @property string $fecha
 */
class Noticias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'noticias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['titulo'],'required'],
            [['fecha'], 'safe'],
            [['titulo'], 'string', 'max' => 50],
            [['texto'], 'string', 'max' => 300],
            [['foto'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_noticia' => 'Id Noticia',
            'titulo' => 'Titulo de la noticia',
            'texto' => 'Texto completo',
            'foto' => 'URL de la Foto',
            'fecha' => 'Fecha',
        ];
    }
}
