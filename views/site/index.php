
<?php
use yii\helpers\Html;
?>
<div class="site-index">
    <div class="jumbotron">
        <h1><?= $noticia->titulo ?></h1>
        <p class="lead"><?= $noticia->texto ?></p>
        <?= Html::img("@web/imgs/$noticia->foto");?>
    </div>
</div>
